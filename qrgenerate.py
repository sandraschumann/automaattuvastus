import cv2
import qrcode
import qrcode.image.svg

# ~ factory = qrcode.image.svg.SvgPathImage

for klass in range(9,13):
    for piirkond in range(1,22):
        for student in range(1,100):
            code = "-".join([ "{:02d}".format(x) for x in [klass, piirkond, student] ])
            print code
            img = qrcode.make(code, box_size=30)#, image_factory=factory)
            img.save("codes/%s.png" % code)
