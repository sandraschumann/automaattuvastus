import cv2
import qrcode
import os
import numpy as np
import sys

img = None
height, width = -1, -1
lastcode = None
pagedict = None

def read_page(number):
    global img, height, width
    
    os.system("convert -density 150 example.pdf[{0}] -quality 90 output.png 2> /dev/null".format(number))
    img = cv2.imread("output.png", 0)
    height, width = img.shape

def read_pagenum_info():
    fin = open("example.csv", 'r')
    t = [ line.split(',') for line in fin.read().split('\n')[1:] if line ]
    fin.close()
    pagedict = {}
    for code, pages in t:
        pagedict[code[1:-1]] = int(pages)
    return pagedict

def detect_qrcode(img):
    qrDecoder = cv2.QRCodeDetector()
    code,_,_ = qrDecoder.detectAndDecode(img)
    if len(code) > 0:
        return code
    return None

def verify_code(newcode, gotpages):
    global lastcode
    
    testcode = lastcode
    lastcode = newcode
    
    if not testcode:
        return None
    
    expectedpages = pagedict[testcode]
    if expectedpages != gotpages:
        print "Wrong number of pages for code %s: expected %d, got %d." % (testcode, expectedpages, gotpages)
        return False
    
    return True

def detect_question(img):
    ret, thresh = cv2.threshold(img, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    # Find contour enclosing 6 questions
    found = False
    for contour in contours:
        x,y,w,h = cv2.boundingRect(contour)
        if abs(float(w)/h - 6) < 1 and abs(float(width)/w - 4) < 1 and abs(float(height)/h - 30) < 5:
            found = True
            break
    
    if not found:
        return -1
    
    questionmeans = []
    for i in range(6):
        questionbox = img[y:y+h, x+w/6*i:x+w/6*(i+1)]
        questionmeans.append(np.mean(questionbox))
    
    for i, q in enumerate(questionmeans):
        without = questionmeans[:i] + questionmeans[i+1:]
        if q < np.mean(without) and np.mean(without) - q > 3*np.std(without):
            return i+1
    
    return -1

def main(args):
    global pagedict
    
    number_of_pages = int(args[1])
    pagedict = read_pagenum_info()
    currentpages = 0
    for i in range(number_of_pages):
        read_page(i)
        code = detect_qrcode(img)
        if code:
            verify_code(code, currentpages)
            print "Code:", code
            currentpages = 0
        else:
            currentpages += 1
            question = detect_question(img[:height/6,width/2:])
            if question > 0:
                print "Question:", question
            else:
                print "Not a meaningful page"
    verify_code(None, currentpages)

if __name__ == '__main__':
    main(sys.argv)

cv2.waitKey(0)
